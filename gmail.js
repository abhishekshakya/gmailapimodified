var express = require('express')
var app = express()

var getTextMessage =require('./getTextMessage');
var getMessageJsonFormat =require('./getMessageJsonFormat');


app.get('/', (req, res) => {
    
  res.send('hello');
})
.use('/text', getTextMessage).use('/json', getMessageJsonFormat);

app.listen(3000);
