var fs = require('fs');
var readline = require('readline');
var { google } = require('googleapis');
var log_file = require('fs').createWriteStream(__dirname + '/log.txt', { flags: 'w' });

var SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];

const TOKEN_PATH = 'token.json';
var gmail = google.gmail('v1');

const getTextMessage = function () {
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            console.log('Error loading client secret file: ' + err);
            return;
        }

        authorize(JSON.parse(content), getRecentEmail);
    });
}

function authorize(credentials, callback) {
    var clientSecret = credentials.installed.client_secret;
    var clientId = credentials.installed.client_id;
    var redirectUrl = credentials.installed.redirect_uris[0];

    var OAuth2 = google.auth.OAuth2;

    var oauth2Client = new OAuth2(clientId, clientSecret, redirectUrl);

    fs.readFile(TOKEN_PATH, function (err, token) {
        if (err) {
            getNewToken(oauth2Client, callback);
        } else {
            oauth2Client.credentials = JSON.parse(token);
            callback(oauth2Client);
        }
    });
}
function getNewToken(oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({ access_type: 'offline', scope: SCOPES });
    console.log('Authorize this app by visiting this url: ', authUrl);
    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.question('Enter the code from that page here: ', function (code) {
        rl.close();
        oauth2Client.getToken(code, function (err, token) {
            if (err) {
                console.log('Error while trying to retrieve access token', err);
                return;
            }
            oauth2Client.credentials = token;
            storeToken(token);
            callback(oauth2Client);
        });
    });
}
function storeToken(token) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token));
    console.log('Token stored to ' + TOKEN_PATH);
}

function hook_stream(stream, callback) {
    var old_write = stream.write

    stream.write = (function (write) {
        return function (string, encoding, fd) {
            write.apply(stream, arguments)
            callback(string, encoding, fd)
        }
    })(stream.write)

    return function () {
        stream.write = old_write
    }
}

var unhook_stderr = hook_stream(process.stderr, function (string, encoding, fd) {
    log_file.write(string, encoding)
})
function getRecentEmail(auth) {

    gmail.users.messages.list({ auth: auth, userId: 'me', maxResults: 10, }, function (err, response) {
        if (err) {
            console.log('The API returned an error: ' + err);
            return;
        }
        for (var i = 0; i < 10; i++) {
            var message_id = response['data']['messages'][i]['id'];
            gmail.users.messages.get({ auth: auth, userId: 'me', 'id': message_id }, function (err, response) {
                if (err) {
                    console.log('The API returned an error: ' + err);
                    return;
                }

                var mimeType = response['data']['payload'].mimeType;

                if (mimeType.includes('multipart')) {
                    message_raw1 = response['data']['payload']['parts'][0].body.data;
                    data1 = message_raw1;
                    buff1 = new Buffer(data1, 'base64');
                    text1 = buff1.toString();
                    var textData1 = text1.replace(/<style([\s\S]*?)<\/style>/gi, ' ')
                        .replace(/<script([\s\S]*?)<\/script>/gi, ' ')
                        .replace(/(<(?:.|\n)*?>)/gm, ' ')
                        .replace(/\s+/gm, ' ');
                    console.error("\n\n<-------------------------New message--------------------------------------->");
                    console.error(textData1);

                }
                else if (mimeType.includes('text')) {

                    const message_raw2 = response['data']['payload'].body.data;
                    data2 = message_raw2;
                    // buff2 = new Buffer(data2, 'base64');
                    buff2 = Buffer.from(data2, 'base64');
                    buff2.allo
                    text2 = buff2.toString();
                    var textData2 = text2.replace(/<style([\s\S]*?)<\/style>/gi, ' ')
                        .replace(/<script([\s\S]*?)<\/script>/gi, ' ')
                        .replace(/(<(?:.|\n)*?>)/gm, ' ')
                        .replace(/\s+/gm, ' ');
                    console.error("\n\n<-------------------------New message--------------------------------------->");
                    console.error(textData2);

                }
            });
        }
    });

}


module.exports = getTextMessage;